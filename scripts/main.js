
class Deck {

  constructor(id, remain, shuffle){
      this.id = id;
      this.remain = remain; 
      this.shuffle = shuffle; 
  }
  
  get getDeck() {
      return this.id;
  }

  get getRemaining(){
      return this.remain;
  }

}


var partieEnCours = true; 
var beginning = true;
const paquet = new Deck();
let score = 0;
var internet; 
var win = 0;
var lose = 0;
var v;



//Manipulation du DOM
      const btnTirer = document.getElementById('tirer');
      const btnDoubler = document.getElementById('doubler');
      const btnMontrer = document.getElementById('montrer');
      const btnBegin = document.getElementById('begin');
      const btnNew = document.getElementById('new');
      const stateCard = document.getElementById("state-card");
      const btnReturnAll = document.getElementById("returnAll");
      const btnReturnOne = document.getElementById("returnOne");
      const btnRejouer = document.getElementById("rejouer");
      const cadreMessage = document.getElementById("cadre");




function BuildDeck() {
    fetch("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1").then(function(response) {
    return response.json();
  }).then(function(deck) {
    
      paquet.id = deck.deck_id;
      paquet.remain = deck.remaining;
      paquet.shuffled = deck.shuffled;

      
      btnBegin.style.display = "none";
      btnNew.style.display = "inline";

      document.getElementById("remain").innerHTML = "Il reste " + paquet.remain +" cartes dans le deck";

  }).catch(function(error){
      console.error(error.message);
  });
}




//on crée une pile vide d
//On va y mettre les cartes pioché -> Draw()
//la derniere carte ajouter à la pile sera cancel quand on veut -> CancelDraw()
//vider la pile dans le deck permet de le réalimenter -> ReturnDeck()
function AddPile() {
  fetch("https://deckofcardsapi.com/api/deck/"+paquet.id+"/pile/cimetiere/add/?cards="+lastDraw).then(function(response) {
  return response.json();
}).then(function(card) {
  
 

}).catch(function(error){
    console.error(error.message);
});
}




async function GameStart(){

    var awaitStart = await fetch("https://deckofcardsapi.com/api/deck/"+paquet.id+"/draw/?count=1");
    var draw = await awaitStart.json();
    partieEnCours = true;
    if (beginning == true)
    {
      var playerHand = document.getElementById('player-hand');
      var createdCard = document.createElement("img");
      createdCard.setAttribute("id", draw.cards[0].code);
      createdCard.setAttribute("class", "drawed");
      createdCard.setAttribute("style", "margin-right : -80;");
    
      
    
      lastDraw = draw.cards[0].code;
      createdCard.src = draw.cards[0].image;
      playerHand.appendChild(createdCard);

        //ajoute carte pioché à la pile 
      AddPile();
      //calcul le score et renvoie dans le label prévu à cet effet
      score = SumCard(score, draw.cards[0].value)
      document.getElementById("sum").innerHTML = score
      //renvoi dans un label le nombre de cartes restantes
    

      paquet.remain = draw.remaining;

      document.getElementById("remain").innerHTML = "Il reste " + paquet.remain +" cartes dans le deck";


      btnTirer.style.display = "inline";
      btnMontrer.style.display = "inline";
      btnReturnAll.style.display = "inline";
      btnReturnOne.style.display = "inline";      
      btnBegin.style.display = "none";
      btnNew.style.display = "none";
      stateCard.style.display = "none";
      cadreMessage.style.display="none";

      beginning = false
    }else{

      await ReturnDeck();
      btnNew.style.display = "none";

    }
    
}


  // Creation des éléments HTML pour la pioche 
  
  let lastDraw;




async function Draw(){

  var dimei = await fetch("https://deckofcardsapi.com/api/deck/"+paquet.id+"/draw/?count=1");
  var draw = await dimei.json();
  
  
  var playerHand = document.getElementById('player-hand');
  var createdCard = document.createElement("img");
  createdCard.setAttribute("id", draw.cards[0].code);
  createdCard.setAttribute("class", "drawed");
  createdCard.setAttribute("style", "margin-right : -80;");

  

  lastDraw = draw.cards[0].code;
  createdCard.src = draw.cards[0].image;
  playerHand.appendChild(createdCard);
  //ajoute carte pioché à la pile 
  AddPile();
  //calcul le score et renvoie dans le label prévu à cet effet
  score = SumCard(score, draw.cards[0].value)
  document.getElementById("sum").innerHTML = score
  //renvoi dans un label le nombre de cartes restantes
  paquet.remain = draw.remaining;
  CheckEmptyDeck(draw.remaining);

  document.getElementById("remain").innerHTML = "Il reste " + paquet.remain +" cartes dans le deck";
  
  if(score==21){
    alert("Victoire Royale \nVous avez obtenu un score de : "+ score);
    window.navigator.vibrate([100,200]);
    v=true;
    await EndGame();
  }else if(score>21){
    alert("Vous dépassez et perdez la partie \nAvec un score de : "+ score);
    v=false;
    window.navigator.vibrate([50,50]);
    EndGame();
  }else if(score<21){
    window.navigator.vibrate(50);
  }

  stateCard.style.display = "none";
  btnNew.style.display = "inline";

}




    // Deux fonction pour retourner la derniere carte dans le deck ne fonctionne pas
function CancelDraw() {
  fetch("https://deckofcardsapi.com/api/deck/"+paquet.id+"/pile/cimetiere/return/?cards="+lastDraw).then(function(response) {
  return response.json();
  }).then(function(cancel){
    
    
    degage = document.getElementById(lastDraw);
    degage.remove()
    //cancel = playerHand.removeChild(id);
    
    //soustraction du score 
    score = SubCard(score, lastDraw)
    document.getElementById("sum").innerHTML = score
    

    paquet.remain = cancel.remaining;
    document.getElementById("remain").innerHTML = "Il reste " + paquet.remain +" cartes dans le deck";
    
    btnTirer.style.display = "inline";
    btnReturnAll.style.display = "inline";
    btnMontrer.style.display = "inline";
    btnRejouer.style.display = "none"
    partieEnCours=true;
    
  }).catch(function(error){
      console.error(error.message);
  })
}  



async function ReturnDeck() {

  var awaitReturn = await fetch("https://deckofcardsapi.com/api/deck/"+paquet.id+"/pile/cimetiere/return/");
  var returnDeck = await awaitReturn.json();
  
    
    document.querySelectorAll(".player-hand img").forEach(img => img.remove());

    score = 0;
    document.getElementById("sum").innerHTML = score;

    paquet.remain = returnDeck.remaining;
    document.getElementById("remain").innerHTML = "Il reste " + paquet.remain +" cartes dans le deck";
    
    stateCard.style.display = "flex"; // on fait reapparaitre le msg table vide

}



async function Rejouer(){

  if(v==true){
    win++;
    
    document.getElementById("V").innerHTML = win;
    
    
  }else if(v==false){
    
    lose++;
    
    document.getElementById("L").innerHTML = lose;

  }

  var awaitStart = await fetch("https://deckofcardsapi.com/api/deck/"+paquet.id+"/draw/?count=1");
  var draw = await awaitStart.json();

  document.querySelectorAll(".player-hand img").forEach(img => img.remove());
  score = 0;
  document.getElementById("sum").innerHTML = score

  var playerHand = document.getElementById('player-hand');
  var createdCard = document.createElement("img");
  createdCard.setAttribute("id", draw.cards[0].code);
  createdCard.setAttribute("class", "drawed");
  createdCard.setAttribute("style", "margin-right : -180;");

  partieEnCours =true; 

  lastDraw = draw.cards[0].code;
  createdCard.src = draw.cards[0].image;
  playerHand.appendChild(createdCard);
  //ajoute carte pioché à la pile 
  AddPile();
  //calcul le score et renvoie dans le label prévu à cet effet
  score = SumCard(score, draw.cards[0].value)
  document.getElementById("sum").innerHTML = score
  //renvoi dans un label le nombre de cartes restantes
  paquet.remain = draw.remaining;
  CheckEmptyDeck(draw.remaining);

  document.getElementById("remain").innerHTML = "Il reste " + paquet.remain +" cartes dans le deck";

      btnTirer.style.display = "inline";
      btnMontrer.style.display = "inline";
      btnReturnAll.style.display = "inline";
      btnReturnOne.style.display = "inline";      
      btnRejouer.style.display = "none";

  
}




// Permet de piocher en appuyant sur D et d'annuler si la touche pressé est C
document.addEventListener('keydown', (event) => {
  
    if(partieEnCours == false){
      if (event.key =='c'|| event.key=='C' && paquet.remaining < 50 ){
          CancelDraw();
        }
    }else{
      if (event.key =='c'|| event.key=='C' && paquet.remaining < 50 ){
          CancelDraw();
        }
        else if (event.key =='d'|| event.key=='D' && paquet.remaining < 51){
          Draw();
        }
    }

  }, 
false);



  //Somme des cartes 
  function SumCard(x,y){
    parseInt(x)
    if(y == "ACE"){
      return x
    }else if(y == 'QUEEN' || y == 'KING'|| y == 'JACK'){
      return x+10
    }else{
      return x+parseInt(y)
    }

  }



  //Soustraire carte
  function SubCard(x,y){
    parseInt(x)
    y.substring(0,1)
    if(y.substring(0,1) == "A"){
      return x
    }else if(y.substring(0,1) == 'Q' || y.substring(0,1) == 'K'|| y.substring(0,1) == 'J'){
      return x-10
    }else{
      return x-parseInt(y.substring(0,1))
    }

  }


//Montrer la main + cas de figure de win/lose
  async function ShowHand(){
    
    if(score<21){
      await Draw();
      
      if(score>21){
        alert("GAGNER \nLa prudence a payé vous auriez pu exploser en faisant : "+ score);
        v=true;
        window.navigator.vibrate([100,200]);
        await EndGame();

      }else
      alert("PERDU \nLa prudence ne paie pas toujours \nVous auriez pu faire : "+ score);
      window.navigator.vibrate([50,50]);
      v=false;
      await EndGame();

      }

}
      
async function EndGame(){

  
  btnTirer.style.display = "none";
  btnReturnAll.style.display = "none";
  btnMontrer.style.display = "none";
  btnRejouer.style.display = "inline";
  btnNew.style.display = "none";
  partieEnCours = false; 
 
  await truc;
  

}

function CheckEmptyDeck(x){
  if(x==0){
    alert ("Il n'y a plus de cartes dans le deck");
    EndGame();
  }
}


window.addEventListener("offline", (event) => {
  const statusDisplay = document.getElementById("status");
  statusDisplay.textContent = "Offline";
  internet = false;
  const padrezo = document.getElementById('network');
  padrezo.setAttribute("src", "image/padrezo");
  


});

window.addEventListener("online", (event) => {
  const statusDisplay = document.getElementById("status");
  statusDisplay.textContent = "Online";
  internet = true;
  const padrezo = document.getElementById('network');
  padrezo.setAttribute("src", "image/rezo.png");
  
});

