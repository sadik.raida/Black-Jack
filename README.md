Black Jack


Règles du jeu : 

Le Poker21 est un jeu de cartes consistant à se rapprocher le plus possible d’un score de 21 qui est obtenu en
additionnant les scores de chaque carte qui sont tirées. 
Le joueur gagne s’il atteint 21, ou s’il décide de s’arrêter et que la
carte suivante est supérieure à 21. Chaque carte vaut sa valeur en point, sauf pour l’As qui vaut 0 point, et le roi, la reine et
le valet qui valent 10 points chacun.



Tutoriel / Instructions de jeu : 

Pour commencer on appuie sur le bouton "Créer un nouveau Deck" que l'on voit à l'écran qui permet la création de votre Deck.

Une fois votre Deck créer vous verrez apparaitre le bouton "Nouvelle partie" depuis lequel vous pourrez lancer la partie et poser votre premiere carte.

S'afficheront ensuite à l'écran des informations comme le nombre de victoire/defaites à votre actif, l'etat de la connexion internet ainsi que votre score qui correspond à la somme des cartes que vous avez posés.

Vous allez pour avancer dans la partie devoir piocher des cartes une par une via le bouton "Tirer". Le but étant d'atteindre 21 ou de trouver le bon timing pour se retirer avant de dépasser la somme de 21. Lorsque vous sentez ce moment où vous allez exploser vous pourrez vous servir du bouton "Montrer". 
Ce bouton va vous montrer la carte que vous auriez pioché. Si cette dernière vous faisait exploser, vous gagnerez. Si elle vous faisait gagner ou rester en dessous de 21, vous perdrez.
Si vous n'etes pas satisfait des cartes qui sortent vous pourrez toujours annuler votre derniere carte pioché avec le bouton "Annuler".
Vous pouvez annuler votre tirage à peu près nimporte quand durant votre partie. Meme lorsque vous explosez, vous aurez la possibilité de revenir en arrière ou d'accepter votre defaite.
Pour accepter votre defaite ou valider votre victoire vous devrez appuyer sur le bouton "Rejouer" qui apparaitra. Ce n'est qu'à ce moment que votre point est comptabilisé comme étant une victoire ou une defaite. 
Au fur et à mesure des parties votre deck s'épuise, mais vous pourrez l'alimenter à tout moment via "Retourner cartes dans le deck" qui remet tout les cartes joués précédemment dans le deck. 
Lorsque le Deck est épuisé vous en pourrez plus piocher, de meme lorsque vous explosez (à moins que vous n'annuliez votre tirage auquel il vous sera de nouveau possible de le faire)